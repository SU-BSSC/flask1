##############################################################
# Author: Warren Sutton
# Date: 8 March 2021
# Purpose: to demonstrate how Flask could be used to create a
# Guest Booking System for Ulumbarra task
##############################################################
from flask import Flask, render_template, request, redirect

app = Flask(__name__)

# This list is could be read from a file or database.
# To simplify the example, just 3 records with 2 fields are added as shown.
guests = [{"name":"warren", "adults":2},
          {"name":"tony", "adults":3},
          {"name":"fred", "adults":4}]


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/listall")
def listall():
    return render_template("listall.html", guests=guests)


@app.route("/remove", methods=["GET","POST"])
def remove():

    if request.method == "POST":
        # need to include code in here to actually delete the record
        return render_template("removedone.html", name=request.form["name"])

    else:
        pass
        # this is never called as a GET so METHODS and function could
        # probably be simplified by removing this and modifying function code.


@app.route("/findremove", methods=["GET","POST"])
def findremove():

    if request.method == "POST":
        req = request.form
        name = req["name"]
        for guest in guests:
            if guest["name"] == name:
                adults = guest["adults"]
                guests.remove(guest) # removes the guest booking
                return render_template("remove.html", name=name, adults=adults)
        return render_template("findremove.html", mess="not found! try again")

    else:
        return render_template("findremove.html")


@app.route("/register", methods=["GET","POST"])
def register():
    
    if request.method == "POST":
        req = request.form
        name = req["name"]
        adults = req["adults"]
        guests.append({"name": name, "adults": adults})
        return render_template("register.html", name=name, adults=adults)
        # return redirect("register.html")

    else:
        return render_template("guestsform.html")


@app.route("/find",  methods=["GET","POST"])
def find():

    if request.method == "POST":
        req = request.form
        name = req["name"]
        for guest in guests:
            if guest["name"] == name:
                adults = guest["adults"]
                return render_template("found.html", name=name, adults=adults)

        return render_template("find.html", mess="not found! try again")
    
    else:
        return render_template("find.html")


if __name__ == "__main__":
    app.run(debug=True)

